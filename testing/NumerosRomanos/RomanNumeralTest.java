import static org.junit.Assert.*;
import org.junit.*;
import java.util.*;
import java.lang.IllegalArgumentException;

public class RomanNumeralTest {
	
	private String numeroRomano;  
	
	// Prueba que I se repita la cantidad de veces correcta
	@Test(expected=IllegalArgumentException.class)
	public void noIRepetidas()
	{
		numeroRomano="IIII";
		RomanNumeral.convierte(numeroRomano);
		
	}
	
	// Prueba que M se repita la cantidad de veces correcta
	@Test(expected=IllegalArgumentException.class)
	public void noMRepetidas()
	{
		numeroRomano="MMMMM";
		RomanNumeral.convierte(numeroRomano);
		
	}
	
	// Prueba que V se repita la cantidad de veces correcta
	@Test(expected=IllegalArgumentException.class)
	public void noVRepetidas()
	{
		numeroRomano="XVV";
		RomanNumeral.convierte(numeroRomano);
		
	}
	
	// Prueba que D se repita la cantidad de veces correcta
	@Test(expected=IllegalArgumentException.class)
	public void noDRepetidas()
	{
		numeroRomano="MDD";
		RomanNumeral.convierte(numeroRomano);
		
	}
	
	// Comprueba que tiene las letras correctas
	@Test(expected=IllegalArgumentException.class)
	public void contieneLetrasRomanos()
	{
		numeroRomano="lafjjkf";
		RomanNumeral.convierte(numeroRomano);
		
	}
	
	// Comprueba que esta I antes que V de forma correcta
	@Test()
	public void iAntesV()
	{
		numeroRomano="XIV";
		assertEquals(14,RomanNumeral.convierte(numeroRomano));
		
	}
	
	// Comprueba que esta I antes que D de forma correcta
	@Test(expected=IllegalArgumentException.class)
	public void iAntesD()
	{
		numeroRomano="XID";
		RomanNumeral.convierte(numeroRomano);
		
	}
	
	// Comprueba que esta X antes que L de forma correcta
	@Test()
	public void xAntesL()
	{
		numeroRomano="CXL";
		assertEquals(140,RomanNumeral.convierte(numeroRomano));
		
	}
	
	// Comprueba que esta X antes que L de forma incorrecta
	@Test(expected=IllegalArgumentException.class)
	public void xAntesl()
	{
		numeroRomano="CXM";
		RomanNumeral.convierte(numeroRomano);
		
	}
	
	// Comprueba que esta V antes que L de forma incorrecta
	@Test(expected=IllegalArgumentException.class)
	public void VL()
	{
		numeroRomano="VL";
		RomanNumeral.convierte(numeroRomano);
		
	}
	
	// Comprueba que esta I entre dos V
	@Test(expected=IllegalArgumentException.class)
	public void VIV()
	{
		numeroRomano="VIV";
		RomanNumeral.convierte(numeroRomano);
		
	}
	
	// Comprueba que hay XX detras de una M
	@Test(expected=IllegalArgumentException.class)
	public void XXM()
	{
		numeroRomano="XXM";
		RomanNumeral.convierte(numeroRomano);
		
	}
    
    	// Comprueba que la entrada esta vacia
	@Test(expected = IllegalArgumentException.class)
	public void testForEmptyInput()
	{
		numeroRomano="";
		RomanNumeral.convierte(numeroRomano);
	}
	
	// Introducimos mas letras de las permitidas
	@Test(expected = IllegalArgumentException.class)
	public void testForOutDomain()
	{
		numeroRomano="AAAA";
		RomanNumeral.convierte(numeroRomano);
	}
	
	// Comprueba que la entrada es nula 
	@Test(expected = NullPointerException.class)
	public void testForNullInput()
	{
		numeroRomano=null;
		RomanNumeral.convierte(numeroRomano);
	}
}
