import static org.junit.Assert.*;
import org.junit.*;
import java.util.*;

public class BisiestoTest {
	
	
	/*Test que implementa la combinacion de B1C1D1 */
	@Test public void siMult4SiMult400SiMult400() {
		int año1 = 400;
		assertEquals("Año no bisiesto", true, Bisiestos.esBisiesto(año1));
	}
	
	/*Test que implementa la combinacion B1C1D2 */
	@Test public void siMult4SiMult400NoMult400() {
		int año2 = 200;
		assertEquals("Año bisiesto", false, Bisiestos.esBisiesto(año2));
	}
	
	/*Test que implementa la combinacion B1C2D2*/
	@Test public void siMult4NoMult400NoMult400() {
		int año3 = 4;
		assertEquals("Año no bisiesto", true, Bisiestos.esBisiesto(año3));
	}
	
	/*Test que implementa la combinacion de B2C2D2 */
	@Test public void noMult4NoMult100NoMult400() {
		int año4 = 1722;
		assertEquals("Año bisiesto", false, Bisiestos.esBisiesto(año4));
	}
	
	/*Test que implementa la combinacion de B3C3D3 */
	@Test(expected=IllegalArgumentException.class)
	public void añoNegativo()
	{
		int año5 = -2;
		Bisiestos.esBisiesto(año5);	
	}
	
	/* Caso extremo */	
	@Test(expected=IllegalArgumentException.class)
	public void añoCero()
	{
		int año6 = 0;
		Bisiestos.esBisiesto(año6);	
	}
}
